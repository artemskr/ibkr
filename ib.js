const util = require('util')
const _ = require('lodash')
const chalk = require('chalk')
const stocks = require('./stocksList')

let globalReqId = 1
const DELAYED = 3 // # Switch to live (1) frozen (2) delayed (3) delayed frozen (4).
const EXPIRATION_DATE = '20200717'
const tickers = {}
const DELTA = -0.32
const LEG_SPREAD = 5
const TIMEOUT_STEP = 5 // sec

const ib = new (require('ib'))({
  clientId: 1,
  host: '127.0.0.1',
  port: 7497
})
  .on('error', function (err) {
    // console.error(chalk.red(err.message))
  })
  .on('tickPrice', function (tickerId, tickType, price, canAutoExecute) {
    // [1]
    const tType = ib.util.tickTypeToString(tickType)
    if (
      tType === 'DELAYED_LAST' &&
      price > 0 &&
      tickers[tickerId].type === 'stock'
    ) {
      ib.emit('onMyEvent', tickers[tickerId].ticker, price)
    }

    if (price > 0 && tickers[tickerId].type === 'options') {
      if (!tickers[tickerId].optionData) {
        tickers[tickerId].optionData = {}
      }

      if (!tickers[tickerId].optionData[tType]) {
        tickers[tickerId].optionData[tType] = {price}
      }
    }
  })

  .on('onMyEvent', function (tradingClass, price) {
    // [2]
    console.log(`${tradingClass} stock price: ` + chalk.yellow(price))
    stocks[tradingClass].price = price
    // lets get list of put option prices below stock

    globalReqId++

    tickers[globalReqId] = {
      type: 'getStrikes',
      ticker: tradingClass
    }

    ib.reqSecDefOptParams(
      globalReqId,
      tradingClass,
      '',
      'STK',
      stocks[tradingClass].conid
    )
  })

  .on('securityDefinitionOptionParameter', function (
    reqId,
    exchange,
    underlyingConId,
    tradingClass,
    multiplier,
    expiration,
    strikes
  ) {
    // [3]
    // here we have list of all strikes for expiration date
    // console.log(
    //   reqId,
    //   exchange,
    //   underlyingConId,
    //   tradingClass,
    //   multiplier,
    //   expiration
    // )
    const exch =
      stocks[tradingClass].exchange !== 'default'
        ? stocks[tradingClass].exchange
        : 'NASDAQOM'
    if (exchange === exch) {
      // console.log(tradingClass, strikes)
      let priceStrike = false
      let priceStrikeIndex = false
      // lest find closest to current price
      for (let i = strikes.length; i >= 0; i--) {
        if (strikes[i] < stocks[tradingClass].price) {
          priceStrike = strikes[i]
          priceStrikeIndex = i
          break
        }
      }
      stocks[tradingClass].priceStrike = priceStrike
      stocks[tradingClass].strikes = strikes
      console.log(
        'closest strike stock price for ' +
          chalk.green(tradingClass) +
          ': ' +
          chalk.yellow(priceStrike)
      )
      // console.log(`priceStrikeIndex ${priceStrikeIndex}`)

      // lets move down 30
      // get all option prices < current market price (priceStrike)
      for (
        let i = priceStrikeIndex - 1;
        i >= priceStrikeIndex - 50 && i >= 0;
        i--
      ) {
        // Option
        globalReqId++
        tickers[globalReqId] = {
          type: 'options',
          ticker: tradingClass,
          strike: strikes[i]
        }

        ib.reqMktData(
          globalReqId,
          ib.contract.option(tradingClass, EXPIRATION_DATE, strikes[i], 'P'),
          '',
          false,
          false
        )
      }
    }
  })

  .on('tickOptionComputation', function (
    tickerId,
    tickType,
    impliedVol,
    delta,
    optPrice,
    pvDividend,
    gamma,
    vega,
    theta,
    undPrice
  ) {
    // [4]
    const tType = ib.util.tickTypeToString(tickType)

    if (!tickers[tickerId].optionData) {
      tickers[tickerId].optionData = {}
    }

    if (!tickers[tickerId].optionData[tType]) {
      tickers[tickerId].optionData[tType] = {}
    }

    tickers[tickerId].optionData[tType] = {
      impliedVol,
      delta,
      optPrice,
      pvDividend,
      gamma,
      vega,
      theta,
      undPrice
    }
  })

ib.startScanning = function () {
  ib.connect()
  ib.reqMarketDataType(DELAYED)

  _.each(stocks, (obj, stock) => {
    // Stock
    tickers[globalReqId] = {
      type: 'stock',
      ticker: stock
    }
    ib.reqMktData(globalReqId++, ib.contract.stock(stock), '', false, false)
  })
}

module.exports = ib

// Disconnect after .. seconds.
setTimeout(function () {
  console.log(chalk.yellow('Cancelling market data subscription...'))

  for (let i = 1; i <= globalReqId; i++) {
    ib.cancelMktData(i)
  }

  console.log(JSON.stringify(tickers))
  go(tickers, stocks)

  ib.disconnect()
}, 7000 + TIMEOUT_STEP * 1000 * (_.keys(stocks).length - 1))

function go(tickers, stocks) {
  // console.log(stocks)
  _.each(stocks, (s, stockName) => {
    let leg1, leg2, cost1, cost2, precise1, precise2
    let delta = false
    // assuming it is in the order of execution (I guess not guaranteed)
    _.each(tickers, t => {
      if (
        t.type === 'options' &&
        t.ticker === stockName &&
        t.optionData &&
        t.optionData.DELAYED_MODEL_OPTION
      ) {
        if (
          t.optionData.DELAYED_MODEL_OPTION.delta > DELTA &&
          delta === false
        ) {
          // console.log(t)
          delta = t.optionData.DELAYED_MODEL_OPTION.delta
          leg1 = t.strike
          leg2 = leg1 - LEG_SPREAD

          const res = getOptionPrice(t.optionData)
          cost1 = res.cost
          precise1 = res.precise
        }
      }
      if (leg2 && t.strike === leg2) {
        const res = getOptionPrice(t.optionData)
        cost2 = res.cost
        precise2 = res.precise
      }
    })
    console.log()
    console.log('Stock name ' + chalk.green(stockName))
    console.log('delta ' + chalk.blue(_.round(delta, 4)))
    console.log('leg1 ' + leg1)
    console.log('leg2 ' + leg2)
    console.log('cost1 ' + cost1)
    console.log('cost2 ' + cost2)
    console.log('precise1 ' + precise1)
    console.log('precise2 ' + precise2)
    const precision = precise1 === true && precise2 === true ? 'GOOD' : 'APPROX'
    console.log(
      `Vertical spread cost ${precision} for ` +
        chalk.green(stockName) +
        ': ' +
        chalk.yellow(_.round(cost1 - cost2, 2))
    )
  })
}

function getOptionPrice(d) {
  let cost = 0
  let precise
  if (
    d.DELAYED_ASK_OPTION &&
    d.DELAYED_ASK_OPTION.optPrice > 0 &&
    d.DELAYED_ASK_OPTION.optPrice !== Infinity &&
    d.DELAYED_BID_OPTION.optPrice > 0 &&
    d.DELAYED_BID_OPTION.optPrice !== Infinity
  ) {
    cost = d.DELAYED_ASK_OPTION.optPrice + d.DELAYED_BID_OPTION.optPrice
    cost = cost / 2
    precise = true
  } else if (
    d.DELAYED_MODEL_OPTION &&
    d.DELAYED_MODEL_OPTION.optPrice > 0 &&
    d.DELAYED_MODEL_OPTION.optPrice !== Infinity
  ) {
    cost = d.DELAYED_MODEL_OPTION.optPrice
    precise = false
  } else if (d.DELAYED_LAST && DELAYED_LAST !== Infinity) {
    cost = d.DELAYED_LAST
    precise = false
  }
  cost = _.round(cost, 2)
  return {cost, precise}
}
