const _ = require('lodash')
const config = require('./db')
const ib = require('./ib')

const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database
  }
})

ib.startScanning()
