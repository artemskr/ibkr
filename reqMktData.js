const util = require('util')
const _ = require('lodash')
const chalk = require('chalk')

const stocks = {
  'SPY': {
    conid: 756733,
    exchange: 'default',
    options: {}
  }
  // 'FB': {
  //   conid: 107113386,
  //   exchange: 'default',
  //   options: {}
  // }
}

let globalReqId = 1
let globalReqIdArray = []
const EXPIRATION_DATE = '20200717'
const tickers = {}
const DELTA = -0.32
const LEG_SPREAD = 5

const ib = new (require('ib'))({
  clientId: 1,
  host: '127.0.0.1',
  port: 7497
})
  .on('error', function (err) {
    console.error(chalk.red(err.message))
  })
  //   .on('result', function (event, args) {
  //   if (!_.includes(['tickEFP', 'tickGeneric', 'tickOptionComputation', 'tickPrice',
  //     'tickSize', 'tickString'], event)) {
  //     console.log('%s %s', chalk.yellow(event + ':'), JSON.stringify(args))
  //   }
  // })
  //   .on('tickEFP', function (tickerId, tickType, basisPoints, formattedBasisPoints,
  //                            impliedFuturesPrice, holdDays, futureExpiry, dividendImpact,
  //                            dividendsToExpiry) {
  //   console.log(
  //     '%s %s%d %s%d %s%s %s%d %s%d %s%s %s%d %s%d',
  //     chalk.cyan(util.format('[%s]', ib.util.tickTypeToString(tickType))),
  //     chalk.bold('tickerId='), tickerId,
  //     chalk.bold('basisPoints='), basisPoints,
  //     chalk.bold('formattedBasisPoints='), formattedBasisPoints,
  //     chalk.bold('impliedFuturesPrice='), impliedFuturesPrice,
  //     chalk.bold('holdDays='), holdDays,
  //     chalk.bold('futureExpiry='), futureExpiry,
  //     chalk.bold('dividendImpact='), dividendImpact,
  //     chalk.bold('dividendsToExpiry='), dividendsToExpiry
  //   )
  // })
  //   .on('tickGeneric', function (tickerId, tickType, value) {
  //   console.log('HERE tickGeneric')
  //   console.log(
  //     '%s %s%d %s%d',
  //     chalk.cyan(util.format('[%s]', ib.util.tickTypeToString(tickType))),
  //     chalk.bold('tickerId='), tickerId,
  //     chalk.bold('value='), value
  //   )
  // })

  //   .on('tickSize', function (tickerId, sizeTickType, size) {
  //   console.log(
  //     '%s %s%d %s%d',
  //     chalk.cyan(util.format('[%s]', ib.util.tickTypeToString(sizeTickType))),
  //     chalk.bold('tickerId:'), tickerId,
  //     chalk.bold('size:'), size
  //   )
  // })
  //   .on('tickString', function (tickerId, tickType, value) {
  //   console.log(
  //     '%s %s%d %s%s',
  //     chalk.cyan(util.format('[%s]', ib.util.tickTypeToString(tickType))),
  //     chalk.bold('tickerId='), tickerId,
  //     chalk.bold('value='), value
  //   )
  // })
  .on('tickPrice', function (tickerId, tickType, price, canAutoExecute) {
    // [1]
    const tType = ib.util.tickTypeToString(tickType)
    if (
      tType === 'DELAYED_LAST' &&
      price > 0 &&
      tickers[tickerId].type === 'stock'
    ) {
      ib.emit('onMyEvent', tickers[tickerId].ticker, price)
    }

    if (price > 0 && tickers[tickerId].type === 'options') {
      if (!tickers[tickerId].optionData) {
        tickers[tickerId].optionData = {}
      }

      if (!tickers[tickerId].optionData[tType]) {
        tickers[tickerId].optionData[tType] = {}
      }

      tickers[tickerId].optionData[tType] = {price}

      // console.log(
      //   '%s %s%d %s%d %s%s',
      //   chalk.cyan(util.format('[%s]', tType)),
      //   chalk.bold('tickerId='),
      //   tickerId,
      //   chalk.bold('price='),
      //   price,
      //   chalk.bold('canAutoExecute='),
      //   canAutoExecute
      // )
    }
  })

  .on('onMyEvent', function (tradingClass, price) {
    // [2]
    console.log(`${tradingClass} stock price: ` + chalk.yellow(price))
    stocks[tradingClass].price = price
    // lets get list of put option prices below stock

    globalReqId++

    tickers[globalReqId] = {
      type: 'getStrikes',
      ticker: tradingClass
    }

    ib.reqSecDefOptParams(
      globalReqId,
      tradingClass,
      '',
      'STK',
      stocks[tradingClass].conid
    )
  })

  .on('securityDefinitionOptionParameter', function (
    reqId,
    exchange,
    underlyingConId,
    tradingClass,
    multiplier,
    expiration,
    strikes
  ) {
    // [3]
    // hee we have list of all strikes for expiration date
    const exch =
      stocks[tradingClass].exchange !== 'default'
        ? stocks[tradingClass].exchange
        : 'NASDAQOM'
    if (exchange === exch) {
      // console.log(tradingClass, strikes)
      let priceStrike = false
      let priceStrikeIndex = false
      // lest find closest to current price
      for (let i = strikes.length; i >= 0; i--) {
        if (strikes[i] < stocks[tradingClass].price) {
          priceStrike = strikes[i]
          priceStrikeIndex = i
          break
        }
      }
      stocks[tradingClass].priceStrike = priceStrike
      stocks[tradingClass].strikes = strikes
      console.log(
        `closest strike stock price for ${tradingClass}: ` +
          chalk.yellow(priceStrike)
      )
      console.log(`priceStrikeIndex ${priceStrikeIndex}`)

      // lets move down 30
      // get all option prices < current market price (priceStrike)
      for (
        let i = priceStrikeIndex - 1;
        i >= priceStrikeIndex - 20 && i >= 0;
        i--
      ) {
        // Option
        globalReqId++
        tickers[globalReqId] = {
          type: 'options',
          ticker: tradingClass,
          strike: strikes[i]
        }
        ib.reqMarketDataType(DELAYED)
        ib.reqMktData(
          globalReqId,
          ib.contract.option(tradingClass, EXPIRATION_DATE, strikes[i], 'P'),
          '',
          false,
          false
        )
      }
    }
  })

  .on('tickOptionComputation', function (
    tickerId,
    tickType,
    impliedVol,
    delta,
    optPrice,
    pvDividend,
    gamma,
    vega,
    theta,
    undPrice
  ) {
    // [4]
    const tType = ib.util.tickTypeToString(tickType)

    if (!tickers[tickerId].optionData) {
      tickers[tickerId].optionData = {}
    }

    tickers[tickerId].optionData[tType] = {
      impliedVol,
      delta, // : _.round(delta, 5),
      optPrice, // : _.round(optPrice, 5),
      pvDividend,
      gamma,
      vega,
      theta,
      undPrice
    }
  })

ib.connect()

const DELAYED = 3 // # Switch to live (1) frozen (2) delayed (3) delayed frozen (4).
ib.reqMarketDataType(DELAYED)

_.each(stocks, (obj, stock) => {
  // Stock
  tickers[globalReqId] = {
    type: 'stock',
    ticker: stock
  }
  ib.reqMktData(globalReqId++, ib.contract.stock(stock), '', false, false)
})

// Disconnect after .. seconds.
setTimeout(function () {
  console.log(chalk.yellow('Cancelling market data subscription...'))

  for (let i = 1; i <= globalReqId; i++) {
    ib.cancelMktData(i)
  }

  console.log(JSON.stringify(tickers))
  go(tickers, stocks)

  ib.disconnect()
}, 20000 * _.keys(stocks).length)

function go(tickers, stocks) {
  // console.log(stocks)
  _.each(stocks, (s, stockName) => {
    console.log('stockName ' + stockName)
    let leg1, leg2, cost1, cost2
    let delta = false
    // assuming it is in the order of execution (I guess not guaranteed)
    _.each(tickers, t => {
      if (
        t.type === 'options' &&
        t.ticker === stockName &&
        t.optionData &&
        t.optionData.DELAYED_MODEL_OPTION
      ) {
        if (
          t.optionData.DELAYED_MODEL_OPTION.delta > DELTA &&
          delta === false
        ) {
          console.log(t)
          delta = t.optionData.DELAYED_MODEL_OPTION.delta
          leg1 = t.strike
          leg2 = leg1 - LEG_SPREAD
          if (
            t.optionData.DELAYED_ASK_OPTION &&
            t.optionData.DELAYED_ASK_OPTION.optPrice > 0 &&
            t.optionData.DELAYED_BID_OPTION.optPrice > 0
          ) {
            console.log(
              't.optionData.DELAYED_ASK_OPTION.optPrice ' +
                t.optionData.DELAYED_ASK_OPTION.optPrice
            )
            console.log(
              't.optionData.DELAYED_BID_OPTION.optPrice ' +
                t.optionData.DELAYED_BID_OPTION.optPrice
            )
            cost1 =
              t.optionData.DELAYED_ASK_OPTION.optPrice +
              t.optionData.DELAYED_BID_OPTION.optPrice
            cost1 = cost1 / 2
          }
        }
      }
      if (leg2 && t.strike === leg2) {
        if (
          t.optionData.DELAYED_ASK_OPTION &&
          t.optionData.DELAYED_ASK_OPTION.optPrice > 0 &&
          t.optionData.DELAYED_BID_OPTION.optPrice > 0
        ) {
          cost2 = _.round(
            (t.optionData.DELAYED_ASK_OPTION.optPrice +
              t.optionData.DELAYED_BID_OPTION.optPrice) /
              2,
            2
          )
        }
      }
    })
    console.log('delta ' + delta)
    console.log('leg1 ' + leg1)
    console.log('leg2 ' + leg2)
    console.log('cost1 ' + cost1)
    console.log('cost2 ' + cost2)
    console.log(
      `Vertical spread cost for ${stockName}: ${_.round(cost1 - cost2, 2)}`
    )
  })
}
