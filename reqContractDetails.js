var _ = require('lodash')
var chalk = require('chalk')

var ib = new (require('ib'))({
  clientId: 1,
  host: '127.0.0.1',
  port: 7497
})
  .on('error', function (err) {
    console.error(chalk.red(err.message))
  })
  .on('result', function (event, args) {
    if (!_.includes(['contractDetails', 'contractDetailsEnd'], event)) {
      console.log('%s %s', chalk.yellow(event + ':'), JSON.stringify(args))
    }
  })
  .on('contractDetails', function (reqId, contract) {
    // console.log(
    //   '%s %s%s %s%s',
    //   chalk.cyan('[contractDetails]'),
    //   'reqId='.bold,
    //   reqId,
    //   'contract='.bold,
    //   JSON.stringify(contract)
    // )

    if (contract)
    console.log(contract)

  })
  .on('contractDetailsEnd', function (reqId) {
    console.log(
      '%s %s%s',
      chalk.cyan('[contractDetailsEnd]'),
      'reqId='.bold,
      reqId
    )
  })
  .on('securityDefinitionOptionParameter', function(reqId, exchange, underlyingConId,
                                                    tradingClass, multiplier, expiration, strikes ) {
    if (exchange === 'NASDAQOM') {
      console.log(reqId, exchange, underlyingConId, tradingClass, multiplier, expiration, strikes)
    }
  })

ib.connect()

// Forex
// ib.reqContractDetails(1, ib.contract.forex('EUR'))
// ib.reqContractDetails(2, ib.contract.forex('GBP'))
// ib.reqContractDetails(3, ib.contract.forex('CAD'))
// ib.reqContractDetails(4, ib.contract.forex('HKD'))
// ib.reqContractDetails(5, ib.contract.forex('JPY'))
// ib.reqContractDetails(6, ib.contract.forex('KRW'))
//
// // Stock
ib.reqContractDetails(11, ib.contract.stock('AAPL'))
// ib.reqContractDetails(12, ib.contract.stock('AMZN'))
// ib.reqContractDetails(13, ib.contract.stock('GOOG'))
// ib.reqContractDetails(14, ib.contract.stock('FB'))

// Option
// ib.reqContractDetails(21, ib.contract.option('SPY', '20200717', 300, 'P'))
// ib.reqContractDetails(22, ib.contract.option('AMZN', '201404', 350, 'P'))
// ib.reqContractDetails(23, ib.contract.option('GOOG', '201406', 1000, 'C'))
// ib.reqContractDetails(24, ib.contract.option('FB', '201406', 50, 'P'))

// ib.reqSecDefOptParams(1235, 'SPY', '', 'STK', 756733)

var NUM_CONTRACTS = 1 // 6 + 4 + 4
var count = 0
ib.on('contractDetailsEnd', function () {
  if (++count >= NUM_CONTRACTS) {
    ib.disconnect()
  }
})

ib.on('securityDefinitionOptionParameterEnd', function () {
  if (++count >= NUM_CONTRACTS) {
    ib.disconnect()
  }
})
