CREATE TABLE IF NOT EXISTS `option_chain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticker`  varchar(5) DEFAULT NULL,
  `right` enum('put','call') DEFAULT NULL,
  `strike` float(10,2) DEFAULT NULL,
  `last` float(10,5) DEFAULT NULL,
  `high` float(10,5) DEFAULT NULL,
  `low` float(10,5) DEFAULT NULL,
  `close` float(10,5) DEFAULT NULL,
  `model_option_id` int(11) DEFAULT NULL,
  `bid_option_id` int(11) DEFAULT NULL,
  `ask_option_id` int(11) DEFAULT NULL,
  `last_option_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT '0000-00-00',
  `updated_at` datetime DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `impliedVol` float(20,17) DEFAULT NULL,
  `delta` float(20,17) DEFAULT NULL,
  `optPrice` float(20,17) DEFAULT NULL,
  `pvDividend` float(20,17) DEFAULT NULL,
  `gamma` float(20,17) DEFAULT NULL,
  `vega` float(20,17) DEFAULT NULL,
  `theta` float(20,17) DEFAULT NULL,
  `undPrice` float(20,17) DEFAULT NULL,
  `created_at` datetime DEFAULT '0000-00-00',
  `updated_at` datetime DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
